
//import Product
const Product = require("../models/Product");


//add product
module.exports.addProduct = (req,res)=>{
	console.log(req.body);

	let newProduct = new Product({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price,

	})

	newProduct.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}
//get active products
module.exports.getActiveProducts = (req,res) => {

	Product.find({isActive: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

//get single product by ID
module.exports.getSingleProduct = (req,res)=>{
	console.log('getSingleProduct');
	console.log(req.params);
	// console.log("getSingleProduct");

	Product.findById(req.params.productId)
		   .then(result => res.send(result))
		   .catch(error => res.send(error))

}

//update product info
module.exports.updateProduct = (req,res) => {

	console.log(req.params.productId);

	console.log(req.body);

	let update = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	}

	Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}
//
module.exports.archiveProduct = (req,res) => {

	let update = {
		isActive: false
	}

	Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

