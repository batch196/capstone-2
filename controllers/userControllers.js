

const User = require("../models/User");
const Order = require("../models/Order");

//import Product
// const Product = require("../models/Product");

const bcrypt = require("bcrypt");

const auth = require("../auth");


module.exports.registerUser = (req,res)=>{
	console.log(req.body);

	const hashedPw = bcrypt.hashSync(req.body.password,10);
	console.log(hashedPw);


	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo,
		email: req.body.email,
		password: hashedPw

	})

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}
module.exports.loginUser = (req,res) => {

	// console.log(req.body);

	User.findOne({email:req.body.email})
	.then(foundUser => {
		// console.log(foundUser);

		if(foundUser === null){
			return res.send({message: "No User Found."})

		} else {
			console.log(foundUser);
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);

			if(isPasswordCorrect){

				return res.send({accessToken: auth.createAccessToken(foundUser)});
			} else {

				return res.send({message: "Incorrect Password"});
			}

		}

	})


}

//set User as admin
module.exports.setAdmin = (req,res) => {
	console.log("i'm here!");
	// console.log(req);

	let update = {isAdmin: req.body.isAdmin}

	User.findByIdAndUpdate(req.params.userId,update,{new:true})
		.then(result => res.send(result))
		.catch(error => res.send({message: "no access"}))

}
//retrieve all active products
module.exports.getActiveCourses = (req,res) => {

	Course.find({isActive: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

//get user details
module.exports.getUserDetails = (req,res)=>{
console.log("I'm here");
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

//
module.exports.myOrders = (req,res)=>{
	Order.find(req.user.userId)
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.allOrders = (req,res)=>{
	Order.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}




