const Order   = require("../models/Order");
const Product = require("../models/Product");
const User    = require("../models/User");

module.exports.addOrder = (req,res)=>{
	//console.log(req.body);

	let newOrder = new Order({
		totalAmount: req.body.totalAmount,
		quantity   : req.body.quantity,
		products   : [{productId: req.body.productId, quantity: req.body.quantity, price : req.body.price}],
		userId 	   : req.body.userId,


	})

	Product.findById(req.body.productId)
		   .then(foundProduct => { 

		   	if(isAdmin = true){
				return res.send({message: "Admins can't place order"})
			}
			
			if(foundProduct === null){
				return res.send({message: "Product not available."})
			}

			newOrder.save()
				    .then(result => res.send({message: "Order Successful"}))
				    .catch(error => res.send(error))
		})
}