const express          = require("express");
const router           = express.Router();
const orderControllers = require("../controllers/orderControllers");
const auth             = require("../auth");

const { verify,verifyAdmin } = auth;

router.post("/addOrder",verify,orderControllers.addOrder);

module.exports = router;