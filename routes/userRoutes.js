const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

const {verify, verifyAdmin} = auth;


//register
router.post("/register",userControllers.registerUser);

//Authentication
router.post('/signin',userControllers.loginUser);

//set user as admin
router.put("/updateUser/:userId",verify,verifyAdmin,userControllers.setAdmin);

//get & put: you accept data from the url
//post & put: you accept data from the body

//get user details
router.get('/details/:userId',verify, verifyAdmin,userControllers.getUserDetails);

//retrieve authenticated user's orders
router.get('/myOrders/:userId',verify,userControllers.myOrders);

//retrieve all orders
router.get('/allOrders/:userId',verify,verifyAdmin,userControllers.myOrders);
module.exports = router;