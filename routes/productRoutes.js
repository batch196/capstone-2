
const express = require("express");
const router = express.Router();

const productControllers = require("../controllers/productControllers");

const auth = require("../auth");

const { verify,verifyAdmin } = auth;



//add new products
router.post("/addProducts",verify,verifyAdmin,productControllers.addProduct);

// //get all active products
router.get('/activeProducts',productControllers.getActiveProducts);

//get single product
router.get('/getSingleProduct/:productId',productControllers.getSingleProduct);

//update product by id
router.put("/updateProduct/:productId",verify,verifyAdmin,productControllers.updateProduct);

//archive product
router.put("/archiveProduct/:productId",verify,verifyAdmin,productControllers.archiveProduct);




module.exports = router;
